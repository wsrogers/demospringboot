package com.allstate.demoSpringBoot.rest;

import com.allstate.demoSpringBoot.entities.Bus;
import com.allstate.demoSpringBoot.service.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/buses")
@CrossOrigin
public class BusController {

    @Autowired
    private BusService busService;

    @GetMapping
    public Collection<Bus> getBuses(){
        return busService.getAllBuses();
    }

    @GetMapping("/{busID}")
    public Bus getBusById(@PathVariable("busId") int id){
        return busService.getBusById(id);
    }

    @PutMapping
    public Bus updateBus(@RequestBody Bus updatedBus){
        return busService.updateBus(updatedBus);
    }
}
