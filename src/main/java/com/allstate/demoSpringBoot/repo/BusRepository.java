package com.allstate.demoSpringBoot.repo;

import com.allstate.demoSpringBoot.entities.Bus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusRepository extends JpaRepository<Bus, Integer> {
}
