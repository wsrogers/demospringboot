package com.allstate.demoSpringBoot.service;

import com.allstate.demoSpringBoot.entities.Bus;

import java.util.Collection;

public interface BusService {
    Collection<Bus> getAllBuses();
    Bus getBusById(int id);

    Bus updateBus(Bus updatedBus);
}
